<?php

class Idea extends Eloquent {
    protected $guarded = array();

    public static $rules = array(
		'mail' => 'required',
		'description' => 'required|badWords'
	);

	public function comments()
	{
		return $this->hasMany('IdeaComment');
	}
}

