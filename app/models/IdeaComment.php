<?php

class IdeaComment extends Eloquent {
    protected $guarded = array();

    public static $rules = array(
    	'email' => 'required',
    	'content' => 'required'
    );

    public function idea()
    {
    	return $this->belongsTo('Idea');
    }
}