<?php

class Message extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'mail' => 'email|required',
		'content' => 'required'
	);
}
