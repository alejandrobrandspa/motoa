<?php

class CalendarEventTableSeeder extends Seeder {

    public function run()
    {
        $calendarevent = array(
        	["title" => "Camapaña suba", "allDay" => false, "start" => new DateTime, "end" => new DateTime]
        );


        // Uncomment the below to run the seeder
        DB::table('events')->insert($calendarevent);
    }

}