<?php

class IdeacommentsTableSeeder extends Seeder {

    public function run()
    {
        $ideacomments = array(
        	[ 'idea_id' => '117', 'name' => 'alexz', 'email' => 'alex@gmail.com', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus distinctio consequuntur ipsam aperiam hic similique numquam doloremque repudiandae. Officiis, repellendus voluptates eos veritatis possimus aperiam vitae ipsa quisquam non animi!'],
        	[ 'idea_id' => '117', 'name' => 'daniel', 'email' => 'alex@gmail.com', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus distinctio consequuntur ipsam aperiam hic similique numquam doloremque repudiandae. Officiis, repellendus voluptates eos veritatis possimus aperiam vitae ipsa quisquam non animi!'],
        	[ 'idea_id' => '117', 'name' => 'vale', 'email' => 'alex@gmail.com', 'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus distinctio consequuntur ipsam aperiam hic similique numquam doloremque repudiandae. Officiis, repellendus voluptates eos veritatis possimus aperiam vitae ipsa quisquam non animi!']
        );

        // Uncomment the below to run the seeder
        DB::table('idea_comments')->insert($ideacomments);
    }

}