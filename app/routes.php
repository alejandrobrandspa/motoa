<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home.index');
});

Route::post('idea/votes', 'IdeasController@votes');

Route::get('gateway/{page}', 'GatewayController@store')->where('page','^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$');

Route::resource('ideas', 'IdeasController');

Route::get('noticias', function(){
	return View::make('blog.index');
});

Route::get('terminos-y-condiciones', function(){
	return View::make('home.terms_conditions');
});

Route::get('post/{id}', function($id){
	return View::make('blog.post')->with('id', $id);
});
/*
|-------------------------------------------------------------------------
|	Api
|-------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api'], function(){
	Route::resource('ideas', 'Api\IdeasController');
	Route::resource('idea/comments', 'Api\IdeaCommentsController');
	Route::resource('tweets', 'Api\TweetsController');
	Route::resource('events', 'Api\EventsController');
	Route::resource('messages', 'Api\MessagesController');
	Route::resource('votes', 'Api\VotesController');
});