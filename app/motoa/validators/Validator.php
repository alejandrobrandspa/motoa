<?php namespace Optima\Services\Validators;

abstract class Validator {

	protected $data;
	protected $rules = [];
	protected $errors = [];
	protected $messages = [];

	public function __construct($data = null) 
	{
		$this->data = $data ?: \Input::all();
	}

	public function passes() 
	{
		$validator = \Validator::make(
				$this->data, 
				$this->rules, 
				$this->messages
		);

		if ($validator->fails()) {
			$this->errors = $validator->messages();
			return false;
		}

		return true;
	}

	public function errors() 
	{
		return $this->errors;
	}


}