<?php namespace Api;

use Input;
use Response;
use Validator;

class IdeaCommentsController extends \BaseController {

    protected $ideaComment;

    public function __construct(\IdeaComment $ideaComment)
    {
        $this->ideaComment = $ideaComment;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $validator = Validator::make($input, \IdeaComment::$rules);
        if ($validator->passes()) {
            $comment = $this->ideaComment->create($input);
            return Response::json($comment);
        }

        return Response::json($validator->messages());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}