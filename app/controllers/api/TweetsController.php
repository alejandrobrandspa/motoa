<?php namespace Api;

use Twitter;
use Response;
use Request;
use Input;
use Validator;

class TweetsController extends \BaseController {

	protected $twitter;

	public function __construct(Twitter $twitter)
	{
		$this->twitter = $twitter;
	}

	public function index($count = 10) 
	{
		return \Twitter::getUserTimeline(array('screen_name' => 'motoasenador', 'count' => $count, 'format' => 'json'));
	}
}