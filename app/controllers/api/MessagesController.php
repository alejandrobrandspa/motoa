<?php namespace Api;

use Validator;
use Input;
use Response;
use Mail;

class MessagesController extends \BaseController {

	protected $message;

	public function __construct(\Message $message)
	{
		$this->message = $message;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, \Message::$rules);

		if ($validation->passes())
		{
			$message = $this->message->create($input);
			$data = ['name' => $message->name, 'mail' => $message->mail,'phone' => $message->phone, 'text' => $message->content];
			Mail::send('emails.message', $data, function($message)
			{
				$message->to('alejandro@brandspa.com', 'Motoa Senador')->subject('Contacto motoa.co');
				$message->to('frank@brandspa.com', 'Motoa Senador')->subject('Contacto motoa.co');
			});
			return Response::json($message);
		}

		return Response::json($validation->errors());
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}