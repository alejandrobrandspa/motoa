<?php namespace Api;

use Validator;
use Input;
use Response;
use Mail;
use Request;

class VotesController extends \BaseController {

	protected $vote;
	protected $idea;

	public function __construct(\Vote $vote, \Idea $idea)
	{
		$this->vote = $vote;
		$this->idea = $idea;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$ip = Request::getClientIp();
		$id = Input::get('idea_id');
		$vote = new \Vote;
 		$vote->idea_id = $id;
 		$vote->ip = $ip;
 		$i = count($this->vote->where(function($query) use($id, $ip){
 			$query->where('idea_id', $id)->where('ip', $ip);
 		})->get());
 		$vote->save();
 		if($vote && $i <= 1){
 			$idea = $this->idea->find($id);
            $new = $idea->increment('votes');
            return Response::json($new);
 		}
 		return Response::json('false');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}