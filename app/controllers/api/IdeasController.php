<?php namespace Api;

use Response;
use Request;
use Input;
use Validator;

class IdeasController extends \BaseController {

    protected $idea;
    protected $vote;

    public function __construct(\Idea $idea, \Vote $vote)
    {
        $this->idea = $idea;
        $this->vote = $vote;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return $this->idea->orderBy("created_at", "desc")->take(10)->get();
    }

     public function paginate()
    {
        return $this->idea->orderBy("created_at", "desc")->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $validation = Validator::make($input, \Idea::$rules);

        if ($validation->passes())
        {
            $idea = $this->idea->create($input);

            return Response::json($idea);
        }

       return Response::json($validation->errors());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->idea->findOrFail($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $ip = Request::getClientIp();
        $vote = new \Vote;
        $vote->idea_id = $id;
        $vote->ip = $ip;
        $i = count($this->vote->where(function($query) use($id, $ip){
            $query->where('idea_id', $id)->where('ip', $ip);
        })->get());
        $vote->save();
        if($vote && $i <= 1) {
            $idea = $this->idea->find($id);
            $new = $idea->increment('votes');
            return Response::json(true, 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->idea->find($id)->delete();
    }

    public function votes() {
        $this->idea->increment('votes');
    }
}