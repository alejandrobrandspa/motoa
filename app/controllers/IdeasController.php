<?php

class IdeasController extends BaseController {
     protected $idea;
     protected $ip;

     public function __construct(Idea $idea, Ip $ip)
     {
        $this->idea = $idea;

        $this->ip = $ip;
    }

    public function index()
    {
        $ideas = $this->idea->with(['comments' => function($query){
            $query->orderBy('id', 'desc');
        }])->orderBy('id', 'desc')->get();
		return View::make('ideas.index', compact('ideas'));
    }

    public function show($id)
    {
    	$idea = $this->idea->find($id);
        $comments = $idea->comments()->orderBy('created_at', 'desc')->get();
    	return View::make('ideas.show', compact('idea'), compact('comments'));
    }


}