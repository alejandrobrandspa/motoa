<?php

class GatewayController extends BaseController {

	public function store($page){

		$g = Gateway::where('page', $page)->first();
		$count = count($g);

		if($count > 0)
		{
			$g->increment('times');
			return View::make('gateway')->with('url', $page);
		} else {
			$gateway = new Gateway;
			$gateway->page = $page;
			$gateway->times = 1;
			$gateway->save();
			return View::make('gateway')->with('url', $page);
		}
	}
}