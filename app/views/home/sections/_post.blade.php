<script id="post-template" type="text/x-handlebars-template">
	<div class="row post" id="post-{{id}}">
		<div class="col-lg-6 col-xs-12 col-md-4">
			<img src="{{thumbnail}}" alt="thumbnail"  class="img-responsive">
		</div>
		<div class="col-lg-6 col-md-8">
			<div class="content">
				<h3><a href="#" class="post-full" data-post-id="{{id}}">{{ title }}</a></h3>
				<span class="created_at">{{dateFormat date}}</span>
				<p>{{{ shortenText excerpt }}}</p>
				<div class="fb-share-button" data-href="http://motoa.co/post/{{id}}" data-type="button"></div>
				<a href="#" class="pull-right post-full btn-see-more" data-post-id="{{id}}"></a>
			</div>
		</div>
	</div>
</script>

<script id="post-full-template" type="text/x-handlebars-template">
	<div class="row">
		<div class="col-lg-12 col-xs-12 col-md-12">
			{{{content}}}
		</div>
	</div>
</script>

<script id="btn-paginate-template" type="text/x-handlebars-template">
	<a href="#/posts/p{{ page }}">Siguiente</a>
</script>




<div class="modal fade post-full-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body"> </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->