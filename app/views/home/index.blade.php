@extends('layouts.main')
@section('header')
<meta property="og:description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
<meta name="description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
<title>Motoa Senador</title>
@stop

@section('content')
<div class="row">
	<div class="col-lg-8 col-md-12">
		<div class="video-container">
		<img src="[[ asset('img/motoa-gracias.png') ]]" class="img-responsive">
			<!-- <iframe src="//www.youtube.com/embed/W4Ar-c_FRJM?autoplay=1" frameborder="0" width="560" height="315"></iframe> -->
		</div>
		<div class="container-posts">
			<div class="loading"><h3>Cargando...</h3></div>
			<div class="posts-list"></div>
			<div class="pull-right" >
				<a href="#" class="btn btn-primary previous-posts" data-count="1">Anterior</a>
				<a href="#" class="btn btn-primary next-posts" data-count="1">Siguiente</a>
			</div>
		</div>
		<div class="visible-sm visible-xs visible-md" style="margin-bottom:90px"></div>
	</div>
@include('sections.sidebar')
@include('sections.ideacomment')
</div>

@stop



