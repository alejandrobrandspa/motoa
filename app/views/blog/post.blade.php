@extends('layouts.main')
@section('header')
	<meta property="og:description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
	<meta name="description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
	<title>Motoa Senador / Noticia</title>
@stop
@section('content')
<div class="col-lg-8">
	<div class="single-post">

	</div>
</div>
@include('sections.sidebar')
@stop

@section('scripts')
<script>
id = [[$id]];
	$.ajax({
		url: 'http://blog.motoa.co/api/get_post',
		 dataType: "jsonp",
		 data: {post_id: id},
		 success: function(response){
		 	console.log(response.post);
		 	$('.single-post').append('<h3>'+response.post.title+'</h3>');
		 	$('.single-post').append(response.post.content);
		 }
	});
</script>
@stop
