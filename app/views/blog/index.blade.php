@extends('layouts.main')
@section('header')
	<meta property="og:description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
	<meta name="description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
	<title>Motoa Senador / Noticias</title>
@stop

@section('content')
<div class="row">
	<div class="col-lg-8 col-md-12">
		<div class="container-blog">
			<div class="posts"></div>
			<a href="#" class="see-more-posts">Ver más</a>

		</div>
	</div>
	@include('sections.sidebar')
</div>

@stop
