<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Motoa Senador</title>
	<link rel="stylesheet" href="[[ asset('vendor/bootstrap/dist/css/bootstrap.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/theme-motoa.css') ]]">
</head>
<body>
	<div class="container">
		<div id="header">
			<h1>Motoa Senador</h1>
			<h2>Cree en tus ideas</h2>
		</div>
		<nav class="menu navbar">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<ul>
				<li><a href="#" class="link_1">Agenda</a></li>
				<li><a href="#" class="link_2">Banco de ideas</a></li>
				<li><a href="#" class="link_3">Blog</a></li>
				<li><a href="#" class="link_4">Plan de gobierno</a></li>
				<li><a href="#" class="link_5">Contáctenos</a></li>
			</ul>
		</nav>

		<div class="row">
			<div class="col-lg-8">
				<div class="video-container">
					<iframe src="//www.youtube.com/embed/8ZYkM3IMkvc" frameborder="0" width="560" height="315"></iframe>
				</div>
				<div class="posts"> </div>
			</div>
			<div class="col-lg-4">
				<div class="sidebar">

					<form id="idea_form_store">
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Nombre">
						</div>
						<div class="form-group">
							<input type="text" name="mail" class="form-control" placeholder="Mail">
						</div>
						<div class="form-group">
							<textarea name="description" id="" cols="30" rows="10" class="form-control" placeholder="Mi idea es"></textarea>
						</div>
						<a href="#" class="btn btn-danger idea-store">ENVIAR</a>
					</form>
					<h3>Conoce las ideas<br> de los colombianos</h3>
					<div class="ideas"></div>
					<div class="tweets"></div>
				</div>
			</div>
		</div>
	</div> <!-- container -->

	<script id="post-template" type="text/x-handlebars-template">
	{{#each posts}}
	<div class="row post">
	<div class="col-lg-8 col-xs-12 col-md-12"><img src="{{thumbnail}}" alt=""  class="img-responsive"></div>
	<div class="col-lg-4">
	<div class="content">
	<h3>{{ title }}</h3>
	{{{ excerpt }}}
	<a href="#" class="btn btn-xs btn-danger pull">ver más</a>
	</div>
	</div>
	</div>
	{{/each}}
	</div>
	</script>

	<script id="tweet-template" type="text/x-handlebars-template">
	{{#each this}}
	<div class="media">
	<a class="pull-left" href="#">
	<img src="{{ user.profile_image_url }}" class="media-object">
	</a>
	<div class="media-body">
	{{ text }}
	</div>
	</div>
	{{/each}}
	</script>

	<script id="idea-template" type="text/x-handlebars-template">
	{{#each this}}
		{{ description }}
		<a href="#" class="btn btn-danger">Apoyar</a>
		<hr>
	{{/each}}
	</script>


</body>
</html>