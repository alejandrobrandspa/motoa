<!doctype html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="UTF-8">
	<meta name="google-site-verification" content="95KJ83_SMvHjO31p6PKqsg30Purmj4-EwfSxezagx0k" />
	@section('header')
	@show

	<link rel="stylesheet" href="[[ asset('css/bootstrap.min.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/font-awesome.min.css') ]]">
	<link rel="stylesheet" href="[[ asset('css/theme-motoa.css') ]]">
	@section('styles')
	@show
</head>
<body id="motoaApp">
	<div class="container">
		<div id="header">
			<div class="row">
				<div class="col-lg-12">
					<a href="#" class="open-menu visible-sm visible-xs visible-md"><img src="[[ asset('img/btn-menu.png') ]]" alt=""></a>
					<a href="/" class="slogan"><img src="[[ asset('img/header-img.png') ]]" alt="cree en tus ideas" style="opacity:0.9" class="img-responsive"></a>
				</div>
			</div>
		</div>
	<div class="visible-sm visible-xs visible-md" style="margin-bottom:30px"></div>
		<nav class="menu navbar hidden-sm hidden-xs hidden-md">
			<ul>
				<li><a href="/" class="linkAgenda"><img src="[[ asset('img/icon-home.png')]]" alt="home" width="24"></a></li>
				<li><a href="#" class="proposal">Nuestra propuesta</a></li>
				<li><a href="/ideas" >Banco de ideas</a></li>
				<li><a href="/noticias">Noticias</a></li>
				<li><a href="#" class="bio" >Biografía</a></li>
				<li><a href="#" class="contact">Contáctenos</a></li>
			</ul>
		</nav>
		<div id="Appcontent">
			@yield('content')
			@include('sections.bio')
			@include('sections.contact')
			@include('sections.proposal')
			@include('sections.place_vote')
			<!-- includes -->
			@include('home.sections._post')
			@include('home.sections._idea')
		</div>

		<footer>
			<p><a href="http://motoa.co/terminos-y-condiciones">Términos y Condiciones</a></p>
			<p class="pull-right">Diseño & Desarrollo by <a href="http://brandspa.com">Brand Spa</a></p>
		</footer>

	</div>

	<div id="sidr">
		<ul>
		<li><a href="/" class="linkAgenda">Inicio</a></li>
				<li><a href="#" class="proposal">Nuestra propuesta</a></li>
				<li><a href="/ideas" >Banco de ideas</a></li>
				<li><a href="/noticias" >Noticias</a></li>
				<li><a href="#" class="bio" >Biografía</a></li>
				<li><a href="#" class="contact">Contáctenos</a></li>
		</ul>
	</div>

	<!-- scripts -->
	<script src="[[ asset('js/libs.min.js') ]]"></script>
	<script src="[[ asset('js/helpers.js') ]]"></script>
	<script src="[[ asset('js/app.js') ]]"></script>
	<script src="[[ asset('js/build.min.js') ]]"></script>
	@section('scripts')
	@show
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-47220043-1', 'motoa.co');
	ga('send', 'pageview');

	</script>
	<div id="fb-root"></div>
	<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=163243910521092";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

	<script type="text/javascript">
	adroll_adv_id = "BWRJMIV2XBHJ7EQ4YP7DYM";
	adroll_pix_id = "GLFA4LUGLRCMHIGZD7A4KW";
	(function () {
		var oldonload = window.onload;
		window.onload = function(){
			__adroll_loaded=true;
			var scr = document.createElement("script");
			var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
			scr.setAttribute('async', 'true');
			scr.type = "text/javascript";
			scr.src = host + "/j/roundtrip.js";
			((document.getElementsByTagName('head') || [null])[0] ||
				document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
			if(oldonload){oldonload()}};
		}());
	</script>
</body>
</html>