<div class="modal fade" id="proposal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><img src="[[ asset('img/icon-proposal.png')]]" width="22"> Nuestra propuesta</h4>
      </div>
      <div class="modal-body">
        <img src="[[  asset('img/img-mt-proposal.jpg')]]" alt="" class="img-responsive">
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  Creo que el poder para cambiar a Colombia, está en los Colombianos que luchan por sus ideas
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body">
               <p>
                Tal vez sea esa empresa propia que has soñado durante largo tiempo, o ese pequeño negocio que ahora mismo sustenta a tu familia, o tal vez ese gran emprendimiento innovador que impactará al mundo.
              </p>
              <p>No importa el tipo de emprendedor que seas, tus ideas valen y pueden cambiar este país,
                por eso trabajaré por leyes que respalden y protejan al emprededor colombiano, porque son tus ideas las que realmente necesita Colombia.
              </p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                ¿Cómo es hoy, el entorno legal para el emprendimiento?
              </a>
            </h4>
          </div>
          <div id="collapseTwo" class="panel-collapse collapse">
            <div class="panel-body">
              <p>
                La Política del Emprendimiento en Colombia tienen aún mucho por ofrecer al emprendedor colombiano, tiene como base cuatro Leyes de la República (Ley 344 de 1996, Ley 590 de 2000, Ley 789 de 2002 y la
                Ley 1014 de 2002), y cuatro Decretos Reglamentarios, sin embargo, solo
                dos de las leyes mencionadas abordan de forma directa los temas de
                “Emprendimiento”, (La Ley 590 de 2000 y la Ley 1014 de 2006) las
                otras dos, solo regulan sutilmente temas de manera esporádica y solo dos
                decretos reglamentarios desarrollan la Ley 1014 de 2006.
              </p>
              <p>
                Es por esto que estoy visitando diferentes regiones del país para
                compartir propuestas para la legislatura comprendida entre los años
                2014 - 2018, la cual busca impulsar y apoyar nuevas políticas que
                ayuden a reducir la informalidad, los altos costos para constituir
                empresas y el difícil acceso al financiamiento y al desarrollo de
                competencias emprendedoras.
              </p>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                ¿Por qué es el momento?
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
             <p>
              Colombia actualmente atraviesa un excelente momento económico, lo
              cual es propicio para impulsar y apoyar la generación y consolidación
              de las micro, pequeñas y medianas empresas, y tú debes creer en tus
              ideas para hacer de Colombia un país más productivo, más innovador
              y más emprendedor, porque son tus ideas las que cambiarán a nuestro país.
            </p>
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
              ¿Cómo trabajar desde el senado por el emprendimiento?
            </a>
          </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
          <div class="panel-body">
            <ul class="with-bullet">
              <li>
                Fortaleceré los programas del Estado enfocados a impulsar
                el emprendimiento de todos los colombianos en el sector
                rural y urbano, con el propósito de promover la articulación
                interinstitucional.
              </li>
              <li>
                Facilitaré las alianzas público privada para impulsar el desarrollo
                empresarial, brindando facilidades de acceso a financiación,
                capacitación, formación y acompañamiento.
              </li>
              <li>
                Promoveré la sostenibilidad de las empresas en el largo plazo,
                a través de la creación de un marco normativo que estimule e
                incentive la creación de empresas, disminuyendo los trámites y
                procedimientos respectivos para su creación y logrando su larga
                perduración en el tiempo.
              </li>
              <li>
                Fomentaré el desarrollo de ruedas de inversión y el acceso
                a servicios financieros, a los proyectos que incorpore la ciencia,
                tecnología y la innovación para estimular la capacidad innovadora
                del sector productivo.
              </li>
            </ul>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
            ¿Cómo puedo aportar?
          </a>
        </h4>
      </div>
      <div id="collapseFive" class="panel-collapse collapse">
        <div class="panel-body">
          <p>
            En mi página www.motoa.co puedes compartir tu idea o inquietud de
            modo que pueda conocer en profundidad las necesidades e ideas de
            los colombianos, juntos podemos avanzar más rápido.
          </p>
          <p>También puedes contactarme a través de mis redes sociales o visitanos en nuestra sede política: Avenida 8 norte No. 20-57 Cali</p>
          <ul class="social" style="padding:10px;height:65px;background:#222">
            <li><a href="https://twitter.com/motoasenador" target="_blank"><img src="[[ asset('img/icon-twt.png') ]]" alt="twitter" ></a></li>
            <li><a href="https://www.facebook.com/MotoaSenador" target="_blank"> <img src="[[ asset('img/icon-fb.png') ]]" alt="facebook" ></a></li>
            <li><a href="mailto:senador@motoa.co" target="_blank"><img src="[[ asset('img/icon-mail.png') ]]" alt="mail" ></a></li>
            <li><a href="http://www.youtube.com/channel/UCWDCHTwTulU1ayOBui_D1cw" target="_blank"><img src="[[ asset('img/icon-yt.png') ]]" alt="youtube" ></a></li>
          </ul>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div>



