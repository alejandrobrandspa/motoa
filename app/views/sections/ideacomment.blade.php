<div class="modal fade" id="idea-comment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><img src="[[ asset('img/icon-contact.png')]]" width="22"> Comentar</h4>
      </div>
      <div class="modal-body">
        <form action="#" class="form">
          <input type="hidden" name="idea_id">
          <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Nombre">
          </div>
          <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
           <textarea name="content" class="form-control" rows="3" placeholder="Mensaje"></textarea>
         </div>
         <button type="button" class="btn store-comment">Enviar</button>
       </form>


     </div>
   </div>
 </div>
</div>