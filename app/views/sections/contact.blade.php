<div class="modal fade" id="contact">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><img src="[[ asset('img/icon-contact.png')]]" width="22"> Contáctenos</h4>
      </div>
      <div class="modal-body">
        <h3>Cuéntanos tus ideas,<br>envíanos un mensaje:</h3>
        <form action="/api/message">
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Nombre">
            </div>
            <div class="form-group">
                <input type="text" name="mail" class="form-control" placeholder="Correo">
            </div>
            <div class="form-group">
                <input type="text" name="phone" class="form-control" placeholder="Teléfono">
            </div>
            <div class="form-group">
                <textarea name="content" class="form-control" rows="5" placeholder="Mensaje"></textarea>
            </div>
            <button class="btn store-message">Enviar</button>
        </form>
        <hr>
        Sede Política: Avenida 8 norte No. 20-57 Cali
        <div class="google-maps">

        </div>
        <hr>
        <h4>Siguenos en nuestras redes sociales</h4>
        <ul class="social">
            <li><a href="https://twitter.com/motoasenador" target="_blank"><img src="[[ asset('img/icon-twt.png') ]]" alt="twitter" ></a></li>
            <li><a href="https://www.facebook.com/MotoaSenador" target="_blank"> <img src="[[ asset('img/icon-fb.png') ]]" alt="facebook" ></a></li>
            <li><a href="mailto:senador@motoa.co" target="_blank"><img src="[[ asset('img/icon-mail.png') ]]" alt="mail" ></a></li>
            <li><a href="http://www.youtube.com/channel/UCWDCHTwTulU1ayOBui_D1cw" target="_blank"><img src="[[ asset('img/icon-yt.png') ]]" alt="youtube" ></a></li>
        </ul>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
