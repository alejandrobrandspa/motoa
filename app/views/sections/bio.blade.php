<div class="modal fade" id="bio">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><img src="[[ asset('img/icon-bio.png')]]" width="22"> Biografía</h4>
      </div>
      <div class="modal-body">
        <img src="[[ asset('img/img-bio.jpg')]]" alt="" class="img-responsive">
        <p class="bio-intro">Desde el principio de mi carrera he tenido la oportunidad de conocer colombianos con buenas ideas, mi compromiso es trabajar desde el congreso para que esas ideas se conviertan en realidad.</p>
        <hr>
        <h2>CARLOS FERNANDO MOTOA</h2>
        <p>Senador de la República de Colombia</p>
        <p>Partido Cambio Radical 2010 - 2014</p>

        <h3>Datos personales</h3>
        <p>Lugar de nacimiento: Palmira, Octubre 23 de 1975. </p>

        <h3>Estudios profesionales</h3>
        <p>Profesión: Abogado de la Universidad San Buenaventura de Cali (1998)</p>

        <h3>Especializaciones</h3>
        <p>Derecho Administrativo ( U. San Buenaventura - U. Pontificia Bolivariana) </p>
        <p>Gestión de Entidades Territoriales ( U. Externado de Colombia) </p>
        <p>Maestría Gobierno Municipal, actualmente (U. Externado de Colombia) </p>

        <h3>Cargos</h3>
        <p>- Jefe de la Unidad Jurídica (Gerencia Territorial Alcaldía de Cali) </p>
        <p>- Jefe de la Unidad Administrativa (Gerencia de desarrollo Territorial de Cali) </p>
        <p>- Director Operativo (Centro de Administración Local Integrado, C.A.L.I No. 6 - Encargado) </p>
        <p>- Secretario de Gobierno del Departamento del Valle del Cauca (2003) </p>
        <p>- Asesor a la Cámara de Representantes (2005) </p>
        <p>- Representante a la Cámara por el Departamento del Valle del Cauca. Periodo (2006 - 2010) </p>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
