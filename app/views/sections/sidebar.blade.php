	<div class="col-lg-4 col-md-12">
		<div class="sidebar">
			<h3>Consulta aquí tu lugar de votación</h3>

			<form  target="_blank" action="http://www.registraduria.gov.co/servicios/censo.htm" class="search-place-vote" >
				<div class="form-group">
				    <input type="text" name="nCedula" class="form-control" placeholder="Ingrese su cédula">
				</div>
				<button class="btn btn-danger pull-right">Consultar</button>
			</form>
			<hr>
			<h4>Síguenos en nuestras redes sociales</h4>
			<ul class="social">
				<li><a href="https://twitter.com/motoasenador" target="_blank"><img src="[[ asset('img/icon-twt.png') ]]" alt="twitter" ></a></li>
				<li><a href="https://www.facebook.com/MotoaSenador" target="_blank"> <img src="[[ asset('img/icon-fb.png') ]]" alt="facebook" ></a></li>
				<li><a href="amilto:senador@motoa.co" target="_blank"><img src="[[ asset('img/icon-mail.png') ]]" alt="mail" ></a></li>
				<li><a href="http://www.youtube.com/channel/UCWDCHTwTulU1ayOBui_D1cw" target="_blank"><img src="[[ asset('img/icon-yt.png') ]]" alt="youtube" ></a></li>
			</ul>
			<div class="fb-like-container">
				<div class="fb-like" data-href="https://www.facebook.com/MotoaSenador" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
			</div>
			<hr>
			@if(Request::segment(1) == 'ideas')

			@else
			<h3>Comparte tu idea</h3>
			<h5>PUEDE SER TU IDEA DE NEGOCIO O UNA PROPUESTA PARA QUE LA TENGAMOS EN CUENTA EN NUESTRA GESTIÓN.</h5>
			<form id="idea_form_store">
				<div class="form-group">
					<input type="text" name="name" class="form-control" placeholder="Nombre">
				</div>
				<div class="form-group">
					<input type="text" name="mail" class="form-control" placeholder="Mail">
				</div>
				<div class="form-group">
					<textarea name="description" id="idea-text" cols="30" rows="4" class="form-control" placeholder="Mi idea es" ></textarea>
				</div>
				<a href="#" class="btn btn-danger pull-right" id="idea_store">ENVIAR</a>
			</form>
			<br>

			<h3>Conoce las ideas<br> de los colombianos</h3>
				<div id="ideas-list"></div>
			@endif
			<h3 class="title-twitter">Motoa en twitter</h3>
			<div class="tweets-list"> </div>
			<p></p>
			<a href="https://twitter.com/intent/tweet?screen_name=motoasenador" class="btn btn-xs btn-primary">Tweet a @senadormotoa</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

			<img src="[[asset('img/logo-cambio.png')]]" alt="cambio radical" class="icon-radical">
		</div>
	</div>

<!-- templates -->
<script id="tweet-template" type="text/x-handlebars-template">
		<div class="media">
			<a class="pull-left" href="#">
				<img src="{{ user.profile_image_url }}" class="media-object">
			</a>
			<div class="media-body">
				<p>{{{urlFormat text }}}</p>
				<span class="created_at">{{dateFormat created_at }}</span>
				<a href="https://twitter.com/intent/retweet?tweet_id={{id_str}}">Retweet</a>
				<a href="https://twitter.com/intent/favorite?tweet_id={{id_str}}">Favorito</a>
			</div>
		</div>
</script>

<script id="idea-template" type="text/x-handlebars-template">
	<p>
		Idea por: {{name}}
		<br>
		{{ description }}
	</p>

	<a href="#" class="btn btn-danger btn-sm vote" data-idea-id="{{id}}"><i class="fa fa-thumbs-up fa-lg"></i> {{votes}}</a>
	<a href="/ideas/{{id}}">ver comentarios</a>
	<a href="#" class="btn-idea-comment" data-idea-id="{{id}}">comentar</a>
</script>









