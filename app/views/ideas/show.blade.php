@extends('layouts.main')
@section('header')
<meta property="og:description" content="[[$idea->description]]">
<meta name="description" content="[[ substr($idea->description, 0, 140) ]]">
<title>Motoa Senador / Idea por [[$idea->name]]</title>
@stop
@section('content')
<div class="row">
	<div class="col-md-8">
		<div class="container-idea-comments">
			<div class="idea">
				<p>
					<span class="by">Idea por: [[$idea->name]]</span>
					[[$idea->description]]
				</p>
				<!-- 			<a href="#" class="btn btn-danger btn-sm vote" data-idea-id="[[$idea->id]]"><i class="fa fa-thumbs-up fa-lg"></i> [[$idea->votes]]</a> -->
				<a href="https://twitter.com/share" class="twitter-share-button pull-left" data-url="http://motoa.co/ideas/[[$idea->id]]" data-text="Yo creo en mis ideas, las comparto y trabajo por ellas" data-lang="es" data-hashtags="CreeEnTusIdeas">Twittear</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<a href="#" class="btn btn-danger btn-sm vote" data-idea-id="[[$idea->id]]"><i class="fa fa-thumbs-up fa-lg"></i> [[$idea->votes]]</a>
				<a href="#" class="btn-idea-comment" data-idea-id="[[ Request::segment(2) ]]">comentar</a>
			</div>
			<br>
			<h3>Comentarios</h3>
			<hr>
			@foreach($comments as $comment)
			<span class="by">comentario por: <i>[[$comment->name]]</i></span>
			[[$comment->content]]
			<br>
			<hr>
			@endforeach
		</div>
	</div>
	@include('sections.sidebar')
</div>

@include('sections.ideacomment')
@stop
