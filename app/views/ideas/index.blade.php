@extends('layouts.main')
@section('header')
	<meta property="og:description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
	<meta name="description" content="Creo que el poder para cambiar a Colombia está en personas que luchan por sus ideas, ya sea un pequeño negocio, una idea digital o una Pyme.">
	<title>Motoa Senador / Ideas</title>
@stop
@section('content')
<div class="row">
	<div class="col-lg-8">
		<div class="banco-ideas-container">
			<h2>Estamos buscando buenas<br> ideas para apoyar, cuéntanos la tuya</h2>
			<h4>
				puede ser tu idea de negocio o una propuesta para
				que la tengamos en cuenta en nuestra gestión.
			</h4>
			<hr>

			<form id="idea_form_store">
				<div class="form-group">
					<input type="text" name="name" class="form-control" placeholder="Nombre">
				</div>
				<div class="form-group">
					<input type="text" name="mail" class="form-control" placeholder="Mail">
				</div>
				<div class="form-group">
					<textarea name="description" id="idea-text" cols="30" rows="4" class="form-control" placeholder="Mi idea es" ></textarea>
				</div>
				<a href="#" class="btn btn-danger pull-right" id="idea_store">ENVIAR</a>
			</form>

			@foreach($ideas as $idea)
			<div class="idea">
				<p>
					<span class="by">Idea por: [[$idea->name]]</span>
					[[$idea->description]]
				</p>
				<a href="https://twitter.com/share" class="twitter-share-button pull-left" data-url="http://motoa.co/ideas/[[$idea->id]]" data-text="Yo creo en mis ideas, las comparto y trabajo por ellas" data-lang="es" data-hashtags="CreeEnTusIdeas">Twittear</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<a href="#" class="btn btn-danger btn-sm vote" data-idea-id="[[$idea->id]]"><i class="fa fa-thumbs-up fa-lg"></i> [[$idea->votes]]</a>
				<a href="/ideas/[[$idea->id]]">Ver comentarios</a>
				<a href="#" class="btn-idea-comment" data-idea-id="[[$idea->id]]">comentar</a>

			</div>
				@if(count($idea->comments) > 0)
					<div class="idea-comments">
						<h3>Comentarios</h3>
						<?php $i = 0 ?>
						@foreach($idea->comments as $comment)
							@if($i < 2)
							<div class="comment">
								<span>Comentario por: [[ $comment->name ]]</span>
								<p>[[[ $comment->content ]]]</p>
							</div>
							@endif
							<?php $i++ ?>
						@endforeach

					</div>
				@endif
			<hr>
			@endforeach

		</div>
	</div>
	<div class="clearfix visible-sm visible-xs"></div>
	@include('sections.sidebar')
	@include('sections.ideacomment')
	<script id="banco-idea-template" type="text/x-handlebars-template">
		<p>{{ description }} </p>
		<a href="#" class="btn btn-danger btn-sm vote" data-idea-id="{{id}}">Apoyar</a>
		<hr>
	</script>
</div>
@stop

