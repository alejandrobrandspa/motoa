module.exports = function(grunt) {

	grunt.initConfig({
		concat: {
			libs: {
				src: [
				"./js/libs/jquery.min.js",
				"./js/libs/handlebars-v1.1.2.js",
				"./js/libs/underscore-min.js",
				"./js/libs/backbone-min.js",
				"./js/libs/bootstrap.min.js",
				"./js/libs/moment.es.js",
				"./js/libs/jquery.sidr.min.js",
				"./js/libs/alertify.min.js",
				"./js/libs/jquery.slimscroll.min.js"
				],
				dest: "./js/libs.js"
			},
			app: {
				src: [
					"./js/app/post.js",
					"./js/app/blog.js",
					"./js/app/idea.js",
					"./js/app/ideaComment.js",
					"./js/app/vote.js",
					"./js/app/tweet.js",
					"./js/app/message.js"

					],
				dest: "./js/build.js"
			}
		},

		uglify: {
			libs: {
				src: "./js/libs.js",
				dest: './js/libs.min.js'
			},
			app: {
				src: "./js/build.js",
				dest: './js/build.min.js'
			}
		}
		});

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask("default", [ "concat", "uglify"]);
   grunt.registerTask("dist", [ "default" ]);
};
