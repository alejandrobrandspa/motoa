/*
|-------------------------------------------------------------------------
|  jquery
|-------------------------------------------------------------------------
*/

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

$.fn.cleanForm = function() {
	var a = this[0].reset();
	return a;
};

    $.fn.characterCounter = function(options){
      
        var defaults = {
            exceeded: false,
            limit: 150,
            counterWrapper: 'span',
            counterCssClass: 'counter',
            counterFormat: '%1',
            counterExceededCssClass: 'exceeded',
            onExceed: function(count) {},
            onDeceed: function(count) {}
        }; 
            
        var options = $.extend(defaults, options);

        return this.each(function() {
            $(this).after('<'+ options.counterWrapper +' class="' + options.counterCssClass + '"></'+ options.counterWrapper +'>');

            bindEvents(this);
            checkCount(this);
        });

        function renderText(count)
        {
            return options.counterFormat.replace(/%1/, count);
        }

        function checkCount(element)
        {
            var characterCount  = $(element).val().length;
            var remaining        = options.limit - characterCount;

            if( remaining < 0 )
            {
                $(element).next("." + options.counterCssClass).addClass(options.counterExceededCssClass);
                options.exceeded = true;
                options.onExceed(characterCount);
            }
            else
            {
                if(options.exceeded) {
                    $(element).next("." + options.counterCssClass).removeClass(options.counterExceededCssClass);
                    options.onDeceed(characterCount);
                    options.exceeded = false;
                }
            }

            $(element).next("." + options.counterCssClass).html(renderText(remaining));
        };    

        function bindEvents(element)
        {
            $(element)
                .bind("keyup", function () { 
                    checkCount(element); 
                })
                .bind("paste", function () { 
                    var self = this;
                    setTimeout(function () { checkCount(self); }, 0);
                });
        }
    };


/*
|-------------------------------------------------------------------------
|  handlebars
|-------------------------------------------------------------------------
*/
Handlebars.registerHelper('dateFormat', function(context, block) {
	if (window.moment && context && moment(context).isValid()) {
		var f = block.hash.format || "MMM Do, YYYY";
		return moment(context).fromNow();
	} else {
        return context;   //  moment plugin is not available, context does not have a truthy value, or context is not a valid date
    }
});

Handlebars.registerHelper('urlFormat', function(text, block) {
	var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
	return text.replace(exp,"<a href='$1' target='_new'>$1</a>");
});

Handlebars.registerHelper('shortenText', function(text, block) {
	a = text.substring(0, 120) + " ...";
	return a;
});

/*
|-------------------------------------------------------------------------
|  backbone
|-------------------------------------------------------------------------
*/
Backbone.events = _.extend({}, Backbone.Events);

template = function(templateElement) {
	return Handlebars.compile(templateElement.html());
};
