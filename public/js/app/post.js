$(function() {
	posts = new Posts();
	post = new Post();
	posts.fetch({dataType: 'jsonp', data:{ page: 1}, reset: true});
	postList = new PostList({collection : posts});
	postFull = new PostModalView({model: post});
	// postRoute = new PostRoute();
	// Backbone.history.start();
});


/*
|-------------------------------------------------------------------------
|	PostRoute
|-------------------------------------------------------------------------
*/
/*PostRoute = Backbone.Router.extend({
	routes: {
		'post/:id': 'post'
	},

	initialize: function(){
		this.model = new Post();
		this.view = new PostModalView();
	},

	post: function(id) {
		promise = this.model.fetch({dataType: 'jsonp', data: {post_id: id}});
		this.view.render(id, promise);
	}
});*/

/*
|-------------------------------------------------------------------------
|	Posts
|-------------------------------------------------------------------------
*/
Post = Backbone.Model.extend({
	url: "http://blog.motoa.co/api/get_post",
	parse: function(response){
		if(response.post){
			this.title = response.post.title;
			return response.post;
		}
		return response;
	}
});

Posts = Backbone.Collection.extend({
	url: "http://blog.motoa.co/api/get_posts",
	model: Post,
	parse: function(response){
		this.page = response.query.page;
		this.pages = response.pages;
		this.count = response.count;
		this.count_total = response.count_total;
		if (response.posts) {
			return response.posts;
		}
		return response;
	}
});

/*
|-------------------------------------------------------------------------
|	View PostView
|-------------------------------------------------------------------------
*/
PostView = Backbone.View.extend({
	template: template($("#post-template")),

	events:{
		'click .post-full' : 'postFull'
	},

	initialize: function(){
		Backbone.events.on("get:postFull", this.postWithRoute);
	},

	render: function() {
		this.$el.html(this.template( this.model.attributes ));
	},

	postWithRoute: function(id){
		console.log(id);
	},

	postFull: function(event) {
		console.log('postView');
		event.preventDefault();
		$that = $(event.currentTarget);
		id = $that.data("postId");
		promise = this.model.fetch({dataType: 'jsonp', data: {post_id: id}});
		Backbone.events.trigger("get:post", promise);
	}
});

/*
|-------------------------------------------------------------------------
|	View PostList
|-------------------------------------------------------------------------
*/
PostList = Backbone.View.extend({
	el: $(".container-posts"),

	events: {
		"click .next-posts": "nextPosts",
		"click .previous-posts": "previousPosts"
	},

	initialize: function() {
		that = this;
		that.list = $(".posts-list");
		that.btnNextPosts = $(".next-posts");
		that.btnPreviousPosts = $(".previous-posts");
		that.loading = $(".loading");
		that.btnPreviousPosts.addClass("disabled");
		that.listenTo(this.collection, "reset", this.render, this);
	},

	render: function() {
		this.list.empty();
		this.loading.fadeIn();
		this.collection.forEach(this.renderOne, this);
		return this;
	},

	renderOne: function(model){
		this.loading.fadeOut();
		postView = new PostView({model: model});
		postView.render();
		this.list.hide().append(postView.el).show('slow');
	},

	nextPosts: function(event) {
		event.preventDefault();
		var $that = $(event.currentTarget);
			that = this;
			page = that.collection.page;
			totalPages = that.collection.pages;
			count = parseInt(page)+1;
		if(page != totalPages) {
			that.btnPreviousPosts.removeClass("disabled");
			that.collection.fetch({dataType: 'jsonp', data: { page: count }, reset: true});
		} else {
			$that.addClass("disabled");
		}
	},

	previousPosts: function(event) {
		event.preventDefault();
		var $that = $(event.currentTarget);
			that = this;
			page = that.collection.page;
			totalPages = that.collection.pages;
			count = parseInt(page)-1;
		if(page == 1 || page >= 0){
			$that.addClass("disabled");
			that.btnNextPosts.removeClass("disabled");
		} else {
			that.collection.fetch({dataType: 'jsonp', data: { page: count }, reset: true});
		}
	}
});

/*
|-------------------------------------------------------------------------
|	View PostModalView
|-------------------------------------------------------------------------
*/
PostModalView = Backbone.View.extend({
	el: $(".post-full-modal"),
	template: template( $("#post-full-template") ),

	initialize: function(){
		that = this;
		Backbone.events.on("get:post", this.render, this);
	},

	render: function(id){
		console.log('renderFull');
		that = this;
		promise.success(function(model, response){
			that.$el.find('.modal-body').html(that.template(model.post));
			that.$el.find('.modal-title').text(model.post.title);
			that.$el.modal();
		});
	}
});