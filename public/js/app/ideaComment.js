var MotoaApp = MotoaApp || {};

MotoaApp.IdeaComment = Backbone.Model.extend({
	urlRoot: '/api/idea/comments'
});

MotoaApp.IdeaComments = Backbone.Collection.extend({
	model: MotoaApp.IdeaComment,
	url: '/idea/comments'
});

MotoaApp.ModalIdeaComment = Backbone.View.extend({
	el: '#idea-comment',
	events: {
		'click .store-comment' : 'storeComment'
	},

	initialize: function(){
		this.form = this.$el.find('form');
		this.id = this.form.find('[name="idea_id"]').val();
	},

	storeComment: function(e){
		e.preventDefault();
		data = this.form.serializeObject();
		model = new MotoaApp.IdeaComment();
		that = this;
		a = model.save(data).done(function(response){
			if (response.created_at) {
				that.$el.modal('hide');
				id = model.get('idea_id');
				window.location = "/ideas/"+id;
			} else {
				$.each(response, function(key, val){
					alertify.error(val);
				});
			}

		});

	},
});

$(function(){
	modalIdeaComment = new MotoaApp.ModalIdeaComment();

	$(document.body).on('click', '.btn-idea-comment',function(e){
		e.preventDefault();
		$this = $(this);
		modal = $('#idea-comment');
		form = modal.find('form');
		form.cleanForm();
		id = $this.data('ideaId');
		form.find('[name="idea_id"]').val(id);
		modal.modal();
	});
});