$(function() {
	blog = new Blog({collection : posts});
});
/*
|-------------------------------------------------------------------------
|	View PostList
|-------------------------------------------------------------------------
*/
Blog = Backbone.View.extend({
	el: $(".container-blog"),

	events: {
		"click .see-more-posts": "nextPosts",
	},

	initialize: function() {
		that = this;
		that.list = $(".posts");
		that.loading = $(".loading");
		that.listenTo(this.collection, "reset", this.render, this);
	},

	render: function() {
		console.log('renderM');
		this.collection.forEach(this.renderOne, this);
		return this;
	},

	renderOne: function(model){

		postView = new PostView({model: model});
		postView.render();
		this.list.append(postView.el);
	},

	nextPosts: function(event) {
		event.preventDefault();
		var $that = $(event.currentTarget);
			that = this;
			page = that.collection.page;
			totalPages = that.collection.pages;
			count = parseInt(page)+1;
			that.collection.fetch({dataType: 'jsonp', data: { page: count }, reset:true});
			if(totalPages == count)
			{
				$('.see-more-posts').fadeOut();
			}
	},

});
