$(function(){
	tweets = new TweetList();
});

Tweet = Backbone.Model.extend({

});

Tweets = Backbone.Collection.extend({
	model: Tweet,
	url: "/api/tweets"
});

/*
|-------------------------------------------------------------------------
|	TweetView
|-------------------------------------------------------------------------
*/

TweetView = Backbone.View.extend({
	template: template($('#tweet-template')),

	initialize: function() {
		this.render();
	},

	render: function() {
		tweet = this.model.toJSON();
		this.$el.html(this.template(tweet));
		return this;
	}
});

/*
|-------------------------------------------------------------------------
|	TweetList
|-------------------------------------------------------------------------
*/

TweetList = Backbone.View.extend({
	el: $(".tweets-list"),

	initialize: function() {
		this.collection = new Tweets();
		this.collection.fetch({reset: true});
		this.listenTo(this.collection, "reset", this.render, this);
	},

	render: function() {
		console.log("render");
		this.collection.each(this.renderOne, this);
		return this;
	},

	renderOne: function(tweet) {
		tweetView = new TweetView({model: tweet});
		this.$el.append(tweetView.el);
	}
});