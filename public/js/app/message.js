// class
Message = function(){

};

// method resp
Message.prototype.resp = function(response, el) {
	if(response.created_at) {
		$(el).parent().fadeOut();
	} else {
		$.each(response, function(key,val){
			alertify.error(val);
		});
	}
};

// method store
Message.prototype.store = function(btn, url) {
	var msg = new Message();
	var _btn = btn;
		_url = url;
	$(document).on('click', _btn, function(e){
		e.preventDefault();
		$this = this;
		el = $(this);
		data = el.parent().serialize();
		$.post(_url, data).done(function(response){
			msg.resp(response, $this);
		});
	});
};

message = new Message();
message.store('.store-message', '/api/messages');