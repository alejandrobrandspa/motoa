//class
Idea = function(){};

Idea.prototype.voteResp = function(response) {
	if(response === true) {
		alertify.success('Gracias por votar');
	} else {
		alertify.error('Usted ya voto por esta idea');
	}
};

Idea.prototype.vote = function(idea) {

	var _idea = idea;

	$(document.body).on('click', '.vote', function(e){
		e.preventDefault();
		$this = $(this);
		id = $this.data('ideaId');
		ideaId = "idea_id="+ $this.data('ideaId');
		console.log('click vote' + ideaId);
		$.ajax({
			type: 'PUT',
			url: '/api/ideas/'+id,
		}).done(function(response){
			_idea.voteResp(response);
		});
	});
};

idea = new Idea();
idea.vote(idea);