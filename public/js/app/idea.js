var MotoaApp = MotoaApp || {};
/*
|-------------------------------------------------------------------------
|	Ideas
|-------------------------------------------------------------------------
*/

MotoaApp.Idea = Backbone.Model.extend({
	urlRoot: '/api/ideas',
});

MotoaApp.Ideas = Backbone.Collection.extend({
	url: '/api/ideas',
	model: MotoaApp.Idea
});

/*
|-------------------------------------------------------------------------
|	IdeaView
|-------------------------------------------------------------------------
*/
MotoaApp.IdeaView = Backbone.View.extend({
	template: template($("#idea-template")),

	events: {

	},

	initialize: function() {
		this.render();
	},

	render: function() {
		idea = this.model.toJSON();
		this.$el.html(this.template( idea ));
		return this;
	},

	showMessage: function(model, response) {
		console.log(response);
		if (response === true) {
			alertify.success("Gracias por votar");
		} else {
			alertify.error("Usted ya participo");
		}
	}
});

/*
|-------------------------------------------------------------------------
|	IdeaList
|-------------------------------------------------------------------------
*/
MotoaApp.IdeaList = Backbone.View.extend({
	el: $("#ideas-list"),

	initialize: function() {
		that = this;
		that.collection = new MotoaApp.Ideas();
		that.collection.fetch({reset: true});
		that.listenTo( this.collection, 'reset', this.render);
		Backbone.events.on('newIdea', this.renderLastIdea, this);
	},

	render: function() {
		this.$el.empty();
		this.collection.each(this.renderIdea, this);
		return this;
	},

	renderIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.hide().append(ideaView.render().el).fadeIn();
		return this;
	},

	renderLastIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.prepend(ideaView.render().el);
		return this;
	}
});

MotoaApp.BancoIdeaList = Backbone.View.extend({
	el: $("#banco-ideas-list"),

	initialize: function() {
		that = this;
		that.collection = new MotoaApp.Ideas();
		that.collection.fetch({reset: true});
		that.listenTo( this.collection, 'reset', this.render);
		Backbone.events.on('newIdea', this.renderLastIdea, this);
	},

	render: function() {
		this.$el.empty();
		this.collection.each(this.renderIdea, this);
		return this;
	},

	renderIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.hide().append(ideaView.render().el).fadeIn();
		return this;
	},

	renderLastIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.prepend(ideaView.render().el);
		return this;
	}
});
/*
|-------------------------------------------------------------------------
|	IdeaStore
|-------------------------------------------------------------------------
*/
MotoaApp.IdeaStore = Backbone.View.extend({
	el: $('#idea_form_store'),

	events: {
		'click #idea_store' : 'addIdea'
	},

	initialize: function() {
		this.model = new MotoaApp.Idea();
		this.collection = new MotoaApp.Ideas();
	},

	workResponse: function(data) {
		console.log(data);
	},

	addIdea: function(e) {
		e.preventDefault();
		that = this;
		data = that.$el.serializeObject();
		if(data.description.length <= 300){
			that.collection.create(data, {wait: true, success: this.storeCallback });
			that.$el.find('.counter').text("300");
			that.$el.cleanForm();
		} else {
			alertify.error("Solo 300 caracteres");
		}

	},

	storeCallback: function(model, response) {
		if (response.created_at) {
			Backbone.events.trigger('newIdea', model);
		} else {
			$.each(response, function(key, val){
				alertify.error(val);
			});
		}
	}
});



$(function() {

	ideaList = new MotoaApp.IdeaList();
	ideaList = new MotoaApp.BancoIdeaList();
	ideaStore = new MotoaApp.IdeaStore();
});