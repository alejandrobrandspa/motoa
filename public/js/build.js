$(function() {
	posts = new Posts();
	post = new Post();
	posts.fetch({dataType: 'jsonp', data:{ page: 1}, reset: true});
	postList = new PostList({collection : posts});
	postFull = new PostModalView({model: post});
	// postRoute = new PostRoute();
	// Backbone.history.start();
});


/*
|-------------------------------------------------------------------------
|	PostRoute
|-------------------------------------------------------------------------
*/
/*PostRoute = Backbone.Router.extend({
	routes: {
		'post/:id': 'post'
	},

	initialize: function(){
		this.model = new Post();
		this.view = new PostModalView();
	},

	post: function(id) {
		promise = this.model.fetch({dataType: 'jsonp', data: {post_id: id}});
		this.view.render(id, promise);
	}
});*/

/*
|-------------------------------------------------------------------------
|	Posts
|-------------------------------------------------------------------------
*/
Post = Backbone.Model.extend({
	url: "http://blog.motoa.co/api/get_post",
	parse: function(response){
		if(response.post){
			this.title = response.post.title;
			return response.post;
		}
		return response;
	}
});

Posts = Backbone.Collection.extend({
	url: "http://blog.motoa.co/api/get_posts",
	model: Post,
	parse: function(response){
		this.page = response.query.page;
		this.pages = response.pages;
		this.count = response.count;
		this.count_total = response.count_total;
		if (response.posts) {
			return response.posts;
		}
		return response;
	}
});

/*
|-------------------------------------------------------------------------
|	View PostView
|-------------------------------------------------------------------------
*/
PostView = Backbone.View.extend({
	template: template($("#post-template")),

	events:{
		'click .post-full' : 'postFull'
	},

	initialize: function(){
		Backbone.events.on("get:postFull", this.postWithRoute);
	},

	render: function() {
		this.$el.html(this.template( this.model.attributes ));
	},

	postWithRoute: function(id){
		console.log(id);
	},

	postFull: function(event) {
		console.log('postView');
		event.preventDefault();
		$that = $(event.currentTarget);
		id = $that.data("postId");
		promise = this.model.fetch({dataType: 'jsonp', data: {post_id: id}});
		Backbone.events.trigger("get:post", promise);
	}
});

/*
|-------------------------------------------------------------------------
|	View PostList
|-------------------------------------------------------------------------
*/
PostList = Backbone.View.extend({
	el: $(".container-posts"),

	events: {
		"click .next-posts": "nextPosts",
		"click .previous-posts": "previousPosts"
	},

	initialize: function() {
		that = this;
		that.list = $(".posts-list");
		that.btnNextPosts = $(".next-posts");
		that.btnPreviousPosts = $(".previous-posts");
		that.loading = $(".loading");
		that.btnPreviousPosts.addClass("disabled");
		that.listenTo(this.collection, "reset", this.render, this);
	},

	render: function() {
		this.list.empty();
		this.loading.fadeIn();
		this.collection.forEach(this.renderOne, this);
		return this;
	},

	renderOne: function(model){
		this.loading.fadeOut();
		postView = new PostView({model: model});
		postView.render();
		this.list.hide().append(postView.el).show('slow');
	},

	nextPosts: function(event) {
		event.preventDefault();
		var $that = $(event.currentTarget);
			that = this;
			page = that.collection.page;
			totalPages = that.collection.pages;
			count = parseInt(page)+1;
		if(page != totalPages) {
			that.btnPreviousPosts.removeClass("disabled");
			that.collection.fetch({dataType: 'jsonp', data: { page: count }, reset: true});
		} else {
			$that.addClass("disabled");
		}
	},

	previousPosts: function(event) {
		event.preventDefault();
		var $that = $(event.currentTarget);
			that = this;
			page = that.collection.page;
			totalPages = that.collection.pages;
			count = parseInt(page)-1;
		if(page == 1 || page >= 0){
			$that.addClass("disabled");
			that.btnNextPosts.removeClass("disabled");
		} else {
			that.collection.fetch({dataType: 'jsonp', data: { page: count }, reset: true});
		}
	}
});

/*
|-------------------------------------------------------------------------
|	View PostModalView
|-------------------------------------------------------------------------
*/
PostModalView = Backbone.View.extend({
	el: $(".post-full-modal"),
	template: template( $("#post-full-template") ),

	initialize: function(){
		that = this;
		Backbone.events.on("get:post", this.render, this);
	},

	render: function(id){
		console.log('renderFull');
		that = this;
		promise.success(function(model, response){
			that.$el.find('.modal-body').html(that.template(model.post));
			that.$el.find('.modal-title').text(model.post.title);
			that.$el.modal();
		});
	}
});
$(function() {
	blog = new Blog({collection : posts});
});
/*
|-------------------------------------------------------------------------
|	View PostList
|-------------------------------------------------------------------------
*/
Blog = Backbone.View.extend({
	el: $(".container-blog"),

	events: {
		"click .see-more-posts": "nextPosts",
	},

	initialize: function() {
		that = this;
		that.list = $(".posts");
		that.loading = $(".loading");
		that.listenTo(this.collection, "reset", this.render, this);
	},

	render: function() {
		console.log('renderM');
		this.collection.forEach(this.renderOne, this);
		return this;
	},

	renderOne: function(model){

		postView = new PostView({model: model});
		postView.render();
		this.list.append(postView.el);
	},

	nextPosts: function(event) {
		event.preventDefault();
		var $that = $(event.currentTarget);
			that = this;
			page = that.collection.page;
			totalPages = that.collection.pages;
			count = parseInt(page)+1;
			that.collection.fetch({dataType: 'jsonp', data: { page: count }, reset:true});
			if(totalPages == count)
			{
				$('.see-more-posts').fadeOut();
			}
	},

});

var MotoaApp = MotoaApp || {};
/*
|-------------------------------------------------------------------------
|	Ideas
|-------------------------------------------------------------------------
*/

MotoaApp.Idea = Backbone.Model.extend({
	urlRoot: '/api/ideas',
});

MotoaApp.Ideas = Backbone.Collection.extend({
	url: '/api/ideas',
	model: MotoaApp.Idea
});

/*
|-------------------------------------------------------------------------
|	IdeaView
|-------------------------------------------------------------------------
*/
MotoaApp.IdeaView = Backbone.View.extend({
	template: template($("#idea-template")),

	events: {

	},

	initialize: function() {
		this.render();
	},

	render: function() {
		idea = this.model.toJSON();
		this.$el.html(this.template( idea ));
		return this;
	},

	showMessage: function(model, response) {
		console.log(response);
		if (response === true) {
			alertify.success("Gracias por votar");
		} else {
			alertify.error("Usted ya participo");
		}
	}
});

/*
|-------------------------------------------------------------------------
|	IdeaList
|-------------------------------------------------------------------------
*/
MotoaApp.IdeaList = Backbone.View.extend({
	el: $("#ideas-list"),

	initialize: function() {
		that = this;
		that.collection = new MotoaApp.Ideas();
		that.collection.fetch({reset: true});
		that.listenTo( this.collection, 'reset', this.render);
		Backbone.events.on('newIdea', this.renderLastIdea, this);
	},

	render: function() {
		this.$el.empty();
		this.collection.each(this.renderIdea, this);
		return this;
	},

	renderIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.hide().append(ideaView.render().el).fadeIn();
		return this;
	},

	renderLastIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.prepend(ideaView.render().el);
		return this;
	}
});

MotoaApp.BancoIdeaList = Backbone.View.extend({
	el: $("#banco-ideas-list"),

	initialize: function() {
		that = this;
		that.collection = new MotoaApp.Ideas();
		that.collection.fetch({reset: true});
		that.listenTo( this.collection, 'reset', this.render);
		Backbone.events.on('newIdea', this.renderLastIdea, this);
	},

	render: function() {
		this.$el.empty();
		this.collection.each(this.renderIdea, this);
		return this;
	},

	renderIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.hide().append(ideaView.render().el).fadeIn();
		return this;
	},

	renderLastIdea: function(model) {
		var ideaView = new MotoaApp.IdeaView({model: model});
		this.$el.prepend(ideaView.render().el);
		return this;
	}
});
/*
|-------------------------------------------------------------------------
|	IdeaStore
|-------------------------------------------------------------------------
*/
MotoaApp.IdeaStore = Backbone.View.extend({
	el: $('#idea_form_store'),

	events: {
		'click #idea_store' : 'addIdea'
	},

	initialize: function() {
		this.model = new MotoaApp.Idea();
		this.collection = new MotoaApp.Ideas();
	},

	workResponse: function(data) {
		console.log(data);
	},

	addIdea: function(e) {
		e.preventDefault();
		that = this;
		data = that.$el.serializeObject();
		if(data.description.length <= 300){
			that.collection.create(data, {wait: true, success: this.storeCallback });
			that.$el.find('.counter').text("300");
			that.$el.cleanForm();
		} else {
			alertify.error("Solo 300 caracteres");
		}

	},

	storeCallback: function(model, response) {
		if (response.created_at) {
			Backbone.events.trigger('newIdea', model);
		} else {
			$.each(response, function(key, val){
				alertify.error(val);
			});
		}
	}
});



$(function() {

	ideaList = new MotoaApp.IdeaList();
	ideaList = new MotoaApp.BancoIdeaList();
	ideaStore = new MotoaApp.IdeaStore();
});
var MotoaApp = MotoaApp || {};

MotoaApp.IdeaComment = Backbone.Model.extend({
	urlRoot: '/api/idea/comments'
});

MotoaApp.IdeaComments = Backbone.Collection.extend({
	model: MotoaApp.IdeaComment,
	url: '/idea/comments'
});

MotoaApp.ModalIdeaComment = Backbone.View.extend({
	el: '#idea-comment',
	events: {
		'click .store-comment' : 'storeComment'
	},

	initialize: function(){
		this.form = this.$el.find('form');
		this.id = this.form.find('[name="idea_id"]').val();
	},

	storeComment: function(e){
		e.preventDefault();
		data = this.form.serializeObject();
		model = new MotoaApp.IdeaComment();
		that = this;
		a = model.save(data).done(function(response){
			if (response.created_at) {
				that.$el.modal('hide');
				id = model.get('idea_id');
				window.location = "/ideas/"+id;
			} else {
				$.each(response, function(key, val){
					alertify.error(val);
				});
			}

		});

	},
});

$(function(){
	modalIdeaComment = new MotoaApp.ModalIdeaComment();

	$(document.body).on('click', '.btn-idea-comment',function(e){
		e.preventDefault();
		$this = $(this);
		modal = $('#idea-comment');
		form = modal.find('form');
		form.cleanForm();
		id = $this.data('ideaId');
		form.find('[name="idea_id"]').val(id);
		modal.modal();
	});
});
//class
Idea = function(){};

Idea.prototype.voteResp = function(response) {
	if(response === true) {
		alertify.success('Gracias por votar');
	} else {
		alertify.error('Usted ya voto por esta idea');
	}
};

Idea.prototype.vote = function(idea) {

	var _idea = idea;

	$(document.body).on('click', '.vote', function(e){
		e.preventDefault();
		$this = $(this);
		id = $this.data('ideaId');
		ideaId = "idea_id="+ $this.data('ideaId');
		console.log('click vote' + ideaId);
		$.ajax({
			type: 'PUT',
			url: '/api/ideas/'+id,
		}).done(function(response){
			_idea.voteResp(response);
		});
	});
};

idea = new Idea();
idea.vote(idea);
$(function(){
	tweets = new TweetList();
});

Tweet = Backbone.Model.extend({

});

Tweets = Backbone.Collection.extend({
	model: Tweet,
	url: "/api/tweets"
});

/*
|-------------------------------------------------------------------------
|	TweetView
|-------------------------------------------------------------------------
*/

TweetView = Backbone.View.extend({
	template: template($('#tweet-template')),

	initialize: function() {
		this.render();
	},

	render: function() {
		tweet = this.model.toJSON();
		this.$el.html(this.template(tweet));
		return this;
	}
});

/*
|-------------------------------------------------------------------------
|	TweetList
|-------------------------------------------------------------------------
*/

TweetList = Backbone.View.extend({
	el: $(".tweets-list"),

	initialize: function() {
		this.collection = new Tweets();
		this.collection.fetch({reset: true});
		this.listenTo(this.collection, "reset", this.render, this);
	},

	render: function() {
		console.log("render");
		this.collection.each(this.renderOne, this);
		return this;
	},

	renderOne: function(tweet) {
		tweetView = new TweetView({model: tweet});
		this.$el.append(tweetView.el);
	}
});
// class
Message = function(){

};

// method resp
Message.prototype.resp = function(response, el) {
	if(response.created_at) {
		$(el).parent().fadeOut();
	} else {
		$.each(response, function(key,val){
			alertify.error(val);
		});
	}
};

// method store
Message.prototype.store = function(btn, url) {
	var msg = new Message();
	var _btn = btn;
		_url = url;
	$(document).on('click', _btn, function(e){
		e.preventDefault();
		$this = this;
		el = $(this);
		data = el.parent().serialize();
		$.post(_url, data).done(function(response){
			msg.resp(response, $this);
		});
	});
};

message = new Message();
message.store('.store-message', '/api/messages');