$(function(){


	/*
	|-------------------------------------------------------------------------
	|	slimScroller
	|-------------------------------------------------------------------------
	*/
	$('.tweets-list').slimScroll({height: "250px", color: '#fff' });

	$('#ideas-list').slimScroll({height: "400px"});


	$("#idea-text").characterCounter({
		limit: '300',
		counterCssClass: 'counter'
	});

	$('.scroll').slimScroll({height: "80px"});
	/*
	|-------------------------------------------------------------------------
	|	bootstrap
	|-------------------------------------------------------------------------
	*/
	$(document.body).on('click', '.bio', function(){
		$('#bio').modal();
	});

	$(document.body).on('click', '.contact', function(){
		contact = $('#contact');
		contact.find('.google-maps').append('<iframe src="https://mapsengine.google.com/map/u/0/embed?mid=zSG2zWd7BWIM.kcBj7wf4v0j8" width="640" height="480"></iframe>');
		contact.modal();
	});

	$(document.body).on('click', '.place-vote', function(){
		$('#place-vote').modal();
	});

	$(document.body).on('click', '.proposal', function(){
		$('#proposal').modal();
	});

	$(document.body).on('click', '.myTab a', function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
	/*
	|-------------------------------------------------------------------------
	|	sidr
	|-------------------------------------------------------------------------
	*/
	$('.open-menu').sidr();

	$('body >.container').on('click', function(){
		$.sidr('close');
	});

	$('#sidr ul li a').on('click', function(){
		$.sidr('close');
	});

});


